package com.simpledemo.firebase;

public class User {
    String name;
    String address;
int id;
    public User() {

    }

    public User(int id,String name, String address) {
        this.name = name;
        this.address = address;
        this.id=id;
    }
    public String getname(){
        return this.name;
    }
    public String getaddress(){
        return this.address;
    }

}

package com.simpledemo.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebasereference;
    EditText name,address,id;
    Button save,read,update,delete;
    User user;
    String TAG="killer";
    public static String userId;
    public  int i=0;
    public int strid=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(EditText)findViewById(R.id.editname);
        address=(EditText)findViewById(R.id.editaddress);
        id=(EditText)findViewById(R.id.editit);
        //
        firebasereference=FirebaseDatabase.getInstance();
        databaseReference=firebasereference.getReference("user");
        // firebasereference.getReference("MyDemo").setValue("Realtimedatabase");
        //userId = databaseReference.push().getKey();
        if (getIntent().getExtras() != null) {
            Intent i=new Intent(this,FullscreenActivity.class);
            startActivity(i);
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        //read data
        read=(Button)findViewById(R.id.readbutton);
        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pol=id.getText().toString();
                //getting reference from user input id
                databaseReference.child(pol).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user=dataSnapshot.getValue(User.class);
                        Toast.makeText(getApplicationContext(),"Name:"+ user.getname()+"   Address:"+user.getaddress(),Toast.LENGTH_SHORT).show();
                        Log.d(TAG, "Name : " + user.getname() + ", Address: " + user.getaddress());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "Failed to read value.");
                    }
                });
            }
        });
        //Update data
        update=(Button)findViewById(R.id.upadtebutton);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String upname=name.getText().toString();
                String upaddress=address.getText().toString();
                String upid=id.getText().toString();
                if(upid.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Enter Id",Toast.LENGTH_SHORT).show();
                }
                else {
                    databaseReference.child(upid).child("name").setValue(upname);
                    databaseReference.child(upid).child("address").setValue(upaddress);
                }
            }
        });
        //delete data
     delete=(Button)findViewById(R.id.deletebutton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String del_id=id.getText().toString();

                if(del_id.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Enter id",Toast.LENGTH_SHORT).show();

                }
                else{
                    System.out.println("=---=");
                    databaseReference.child(del_id).removeValue();}
            }
        });

        //save data
        save=(Button)findViewById(R.id.savebutton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        Intent i=new Intent(getApplicationContext(),Insert.class);
                startActivity(i);
            }
        });

    }

}

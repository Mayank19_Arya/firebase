package com.simpledemo.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class Insert extends AppCompatActivity {
FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    EditText name,address,id;
    Button save;
    String TAG="tagcheck";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        firebaseDatabase=FirebaseDatabase.getInstance();            //firebase instance initialisation
        databaseReference=firebaseDatabase.getReference("user");

        name=(EditText)findViewById(R.id.editname);
        address=(EditText)findViewById(R.id.editaddress);
        id=(EditText)findViewById(R.id.editit);

//getting token using button
        Button logTokenButton = (Button) findViewById(R.id.tokenget);
        logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get token
                String token = FirebaseInstanceId.getInstance().getToken();

                // Log and toast
                String msg = getString(R.string.msg_token_fmt, token);

                Log.d(TAG, msg);
                System.out.println("Messaging Service : "+msg);

                Toast.makeText(Insert.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
        Button subscribeButton = (Button) findViewById(R.id.subscribe);
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // [START subscribe_topics]
                FirebaseMessaging.getInstance().subscribeToTopic("news");
                // [END subscribe_topics]

                // Log and toast
                String msg = getString(R.string.msg_subscribed);
                Log.d(TAG, msg);
                Toast.makeText(Insert.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
//saving user iput details on buttonclick
        save=(Button)findViewById(R.id.savebutton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             int id=0;
               String userId = databaseReference.push().getKey();
                final String oname = name.getText().toString();
                final String oaddress = address.getText().toString();
                System.out.println("user id push.getkey  ="+userId);
                final User user=new User(id,oname,oaddress);
               // databaseReference.child(userId).setValue(user);

                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Object value= dataSnapshot.getValue();
                        String i= String.valueOf(dataSnapshot.getChildrenCount());
                        int id= Integer.parseInt(i);    //getting the current children count
                        System.out.println("--------"+i+"------"+id);
                        final User user=new User(id,oname,oaddress);       //putting new data on current count
                       // dataSnapshot.getRef().setValue(user);
                        databaseReference.child(String.valueOf(i)).setValue(user);  //setting value

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("cancelled");
                    }
                });
            }});
            }
}

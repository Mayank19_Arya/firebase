package com.simpledemo.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MyFirebaseMessagingService  extends FirebaseMessagingService {
String TAG="tagmessage";

//overriding onmessgaerecieved which is executed when anotification is recieved
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.v(TAG,"from: "+remoteMessage.getFrom());
        if(remoteMessage.getData().size()>0){
            Log.v(TAG,"Message payload:"+ remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Log.v(TAG,"Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        sendNotification(remoteMessage);
    }
//seniding notification when app is in foreground
   private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent=new Intent(this,FullscreenActivity.class);
        PendingIntent pendingIntent=PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle("Notification");
        notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
        notificationBuilder.setSmallIcon(R.mipmap.bar);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setAutoCancel(true);
        NotificationManager notificationManager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,notificationBuilder.build());
    }
    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "Service Created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "Service Destroyed...");
    }
 

}
